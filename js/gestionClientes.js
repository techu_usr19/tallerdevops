var clientesObtenidos;

// Todos los clientes de Alemania
function getClientes () {
    var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest ();
    request.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
          console.log(request.responseText);
          clientesObtenidos = request.responseText;
          procesarClientes ();
      }
    }
    request.open ("GET", url, true);
    request.send();
}

function procesarClientes () {
    var JSONClientes = JSON.parse (clientesObtenidos);
    var tabla = document.getElementById ("tablaClientes");
  
    for (var i = 0; i < JSONClientes.value.length; i++) {
        console.log(JSONClientes.value[i].ProductName);
        var nuevafila = document.createElement("tr");
        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = JSONClientes.value[i].ContactName;
        var columnaCiudad = document.createElement("td");
        columnaCiudad.innerText = JSONClientes.value[i].City;
        var columnaTelefono = document.createElement("td");
        columnaTelefono.innerText = JSONClientes.value[i].Phone;
        var columnaPais = document.createElement("td");
        columnaPais.innerText = JSONClientes.value[i].Country;
        var columnaPaisImg = document.createElement("td");
        var imgPais = document.createElement("img");
        if (JSONClientes.value[i].Country == 'UK') {
            imgPais.setAttribute("src", "https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png");
        }
        else {
            imgPais.setAttribute("src", "https://www.countries-ofthe-world.com/flags-normal/flag-of-" + JSONClientes.value[i].Country + ".png");    
        }
        //imgPais.setAttribute("height", "20%");
        imgPais.setAttribute("class", "flag");
        columnaPaisImg.appendChild (imgPais);
        nuevafila.appendChild (columnaNombre);
        nuevafila.appendChild (columnaCiudad);
        nuevafila.appendChild (columnaTelefono);
        nuevafila.appendChild (columnaPais);
        nuevafila.appendChild (columnaPaisImg);
        tabla.appendChild(nuevafila);
    }
  }
  